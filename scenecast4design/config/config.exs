# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :scenecast4design,
  ecto_repos: [Scenecast4design.Repo]

# Configures the endpoint
config :scenecast4design, Scenecast4designWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EqJcM7Ya2jw+pVjC+Bi1kRXTByLeRsc+WcsDfBbjWNHn+PvKtTT1Hi+qaPTUAduI", # ToDo: blackbox this file
  render_errors: [view: Scenecast4designWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Scenecast4design.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Guardian
config :scenecast4design, Scenecast4design.Auth.Guardian,
  issuer: "scenecast4design",
  secret_key: "P8Pq6yjGHnhKu1ZpuBt4eL6GjgKOEHhiPk+ZFvrK3uLtmUDNr2MufeRvKVi0LfIW" # ToDo: blackbox this file

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures ExAdmin
config :ex_admin,
  repo: Scenecast4design.Repo,
  module: Scenecast4design,
  modules: [
    Scenecast4design.ExAdmin.Dashboard,
  ]

# Set default locale
config :gettext, :default_locale, "en"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :xain, :after_callback, {Phoenix.HTML, :raw}

