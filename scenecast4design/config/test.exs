use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :scenecast4design, Scenecast4designWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :scenecast4design, Scenecast4design.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "scenecast4design_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# configure bcrypt tests number of rounds
config :bcrypt_elixir, :log_rounds, 4
