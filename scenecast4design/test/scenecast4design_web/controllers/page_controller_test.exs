defmodule Scenecast4designWeb.PageControllerTest do
  use Scenecast4designWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Scenecast4design!"
  end
end
