defmodule Scenecast4designWeb.NavigationTest do
  use Scenecast4designWeb.ConnCase

  test "shows a sign in with link when not signed in", %{conn: conn} do
    conn = get conn, "/"

    assert html_response(conn, 200) =~ "Sign in"
  end
end
