defmodule Scenecast4design.Auth.Guardian do
  use Guardian, otp_app: :scenecast4design

  alias Scenecast4design.Auth

  def subject_for_token( user, _claims) do
    {:ok, to_string(user.id)}
  end

  def resource_from_claims(claims) do
    user = claims[ "sub"]
    |> Auth.get_user!
    {:ok, user}

    # If sthg. goes wrong here return {:erro, reason}
  end
end
