defmodule Scenecast4design.Auth.Pipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :scenecast4design,
    error_handler: Scenecast4design.Auth.ErrorHandler,
    module: Scenecast4design.Auth.Guardian

  # If there is a session token, validate it
  plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}

  # If there is an authorization header, validate it
  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}

  # Load the user, if either of the verifications worked
  plug Guardian.Plug.LoadResource, allow_blank: true
end
