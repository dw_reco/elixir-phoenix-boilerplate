defmodule Scenecast4designWeb.Router do
  use Scenecast4designWeb, :router
  use ExAdmin.Router

  pipeline :auth do
    plug Scenecast4design.Auth.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end


  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end


  # Other scopes may use custom stacks.
  # scope "/api", Scenecast4designWeb do
  #   pipe_through :api
  # end
  # Maybe logged in scope
  scope "/", Scenecast4designWeb do
    pipe_through [:browser, :auth]

    get "/", PageController, :index
    post "/", PageController, :login
    post "/logout", PageController, :logout
  end

  # Definitely logged in scope
  scope "/", Scenecast4designWeb do
    pipe_through [:browser, :auth, :ensure_auth]

    get "/secret", PageController, :secret
  end

  # your app's routes
  scope "/admin", ExAdmin do
    pipe_through :browser
    admin_routes()
  end
end
