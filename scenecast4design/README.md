# Scenecast4design

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`


## Getting started

### Start server
```
iex -S mix phx.server
```

### Insert new user
This is an example user called "dev".

```
iex(1)> Scenecast4design.Auth.create_user(%{username: "dev", password: "devdevdev", email: "dev@example.org"})
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser and log in.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
